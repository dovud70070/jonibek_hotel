class Room:
    def __init__(self, room_number, room_name, price, room_id=None, is_busy=False):
        self.room_id = room_id
        self.room_number = room_number
        self.room_name = room_name
        self.price = price
        self.is_busy = is_busy

    def __str__(self):
        return f"{self.room_number}  {self.room_name} {self.price} {self.is_busy}"
