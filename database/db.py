import psycopg2
from room import *
from book import *
from client import *


def create_room(room):
    """ insert a new vendor into the vendors table """
    sql = """INSERT INTO room(room_number,room_name,price, is_busy )
             VALUES(%s, %s, %s, %s) RETURNING room_id, room_number, room_name, price, is_busy;"""
    conn = None
    room_model = None
    try:
        # read database configuration
        # params = config()
        # connect to the PostgreSQL database
        conn = psycopg2.connect(host="localhost",
                                database="hotel",
                                user="hotel",
                                password="hotel")
        # create a new cursor
        cur = conn.cursor()
        # execute the INSERT statement
        print(room.room_name)
        cur.execute(sql, (room.room_number, room.room_name, room.price, room.is_busy))
        # get the generated id back
        data = cur.fetchone()
        # print(type(tuple(cur.fetchone())))
        room_model = Room(room_number=data[1], is_busy=[4], room_name=data[2], price=data[3], room_id=data[0])
        # commit the changes to the database
        conn.commit()
        # close communication with the database
        cur.close()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()
    return room_model


def create_client(client):
    sql = """
    INSERT INTO client(full_name, phone_number, email)
    VALUES(%s, %s, %s) RETURNING client_id, full_name, phone_number, email;
    """
    conn = None
    client_model = None
    try:

        conn = psycopg2.connect(host="localhost",
                                database="hotel",
                                user="hotel",
                                password="hotel")
        # create a new cursor
        cur = conn.cursor()
        cur.execute(sql, (client.full_name, client.phone_number, client.email))
        data = cur.fetchone()
        # print(type(tuple(cur.fetchone())))
        client_model = Client(client_id =data[0], full_name=[1], phone_number = data[2], email=data[3])
        # commit the changes to the database
        conn.commit()
        # close communication with the database
        cur.close()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)

    finally:
        if conn is not None:
            conn.close()
    return client_model


def book_room(book):
    """ Insert a new book into the book table """
    sql = """
    INSERT INTO book(client, room, start_date, end_date)
    VALUES (%s, %s, %s, %s) RETURNING book_id, client, room, start_date, end_date;
    """

    conn = None
    book_model = None

    try:
        conn = psycopg2.connect(host="localhost",
                                database="hotel",
                                user="hotel",
                                password="hotel")
        # create a new cursor
        cur = conn.cursor()
        cur.execute(sql, (book.client, book.room, book.start_date, book.end_date))
        data = cur.fetchone()
        # print(type(tuple(cur.fetchone())))
        book_model = Book(client=data[1], room=data[2], start_date=data[3], end_date=data[4])
        # commit the changes to the database
        conn.commit()
        # close communication with the database
        cur.close()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()
    return book_model
