class Client:
    def __init__(self, full_name, phone_number, email, client_id=None):
        self.client_id = client_id
        self.full_name = full_name
        self.phone_number = phone_number
        self.email = email

    def __str__(self):
        return f"{self.client_id}"
