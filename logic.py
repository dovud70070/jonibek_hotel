from book import Book
from client import Client
from room import Room

from database.db import create_room

room_name = input("Room name : ")
price = input("Room price : ")
room_number = input("Room number : ")
is_busy = input("is busy Room? type true or false :")

room = create_room(Room(room_number=room_number, room_name=room_name, price=price, is_busy=is_busy))
print(room)
