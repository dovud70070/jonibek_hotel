class Book:

    def __init__(self, client, room, start_date, end_date, book_id=None):
        self.book_id = book_id
        self.client = client
        self.room = room
        self.start_date = start_date
        self.end_date = end_date

    def __str__(self):
        return self.client + " " + self.room
