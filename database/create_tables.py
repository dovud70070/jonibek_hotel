
import psycopg2
from config import config


def create_tables():
    """ create tables in the PostgreSQL database"""
    commands = (
        """
        
        CREATE TABLE room (
            room_id SERIAL PRIMARY KEY,
            room_number INTEGER UNIQUE NOT NULL,
            room_name VARCHAR(255) NOT NULL,
            price FLOAT NOT NULL,
            is_busy BOOLEAN NOT NULL
            
        )
        """,
        """
        
        CREATE TABLE client (
                client_id SERIAL PRIMARY KEY,
                full_name VARCHAR(255) NOT NULL,
                phone_number VARCHAR(20) NOT NULL,
                email text NOT NULL
                )
        """,
        """
        
        CREATE TABLE book (
                book_id SERIAL PRIMARY KEY,
                client INTEGER NOT NULL,
                room INTEGER NOT NULL,
                FOREIGN KEY (client)
                REFERENCES client (client_id)
                ON UPDATE CASCADE ON DELETE CASCADE,
                FOREIGN KEY (room)
                REFERENCES room (room_id)
                ON UPDATE CASCADE ON DELETE CASCADE,
                start_date DATE NOT NULL,
                end_date DATE NOT NULL
        )
        """
    )
    conn = None
    try:
        # read the connection parameters
        # params = config()
        # connect to the PostgreSQL server
        conn = psycopg2.connect(host="localhost",
        database="hotel",
        user="hotel",
        password="hotel")
        cur = conn.cursor()
        # create table one by one
        for command in commands:
            cur.execute(command)
        # close communication with the PostgreSQL database server
        cur.close()
        # commit the changes
        conn.commit()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()


if __name__ == '__main__':
    create_tables()
