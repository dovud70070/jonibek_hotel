import psycopg2


# from config import config


def get_client():
    """ query data from the vendors table """
    conn = None

    try:
        conn = psycopg2.connect(host="localhost",
                                database="hotel",
                                user="hotel",
                                password="hotel")
        cur = conn.cursor()
        cur.execute("SELECT client_id, full_name, phone_number, email FROM client ORDER BY client_id")
        print("The amount of rooms", cur.rowcount)
        row = cur.fetchone()

        while row is not None:
            print(f"Id {row[0]} Full Name {row[1]} Phone Number {row[2]} Email {row[3]}")
            row = cur.fetchone()

        cur.close()

    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()


def get_client_with_email(email):
    """ query data from the vendors table """
    conn = None
    client_id = None
    try:
        conn = psycopg2.connect(host="localhost",
                                database="hotel",
                                user="hotel",
                                password="hotel")
        cur = conn.cursor()
        print(email)
        postgreSQL_select_Query = "SELECT * FROM client where email = %s;"
        print(postgreSQL_select_Query)
        cur.execute(postgreSQL_select_Query, (email,))
        row = cur.fetchone()
        client_id = row[0]
        while row is not None:
            print(f"Id {row[0]} Number {row[1]} Name {row[2]}")
            row = cur.fetchone()

        cur.close()

    except (Exception, psycopg2.DatabaseError) as error:
        print("error", error)
    finally:
        if conn is not None:
            conn.close()
    return client_id


if __name__ == "__main__":
    get_client()
