# ALIFTECH TASK


## Setup

### DATABASE
 - USE POSTGRESQL 
```
  open terminal and type command
  
  sudo -i -u postgres
  
  creaete databse with owner
  
  create role hotel with login password "hotel";
  create database hotel with owner hotel;
  
  well done you created dabase 
```
 - Install virtual environment:
```
git clone git@github.com:clyqe/clyqe-backend.git

python3.8 -m venv --prompt="v" .env

pip install -r requirements.txt
```


- create tables:

```
python3 database/create_tables.py

```

- insert into data in tables
```
Room tabel
python3 logic.py

```
- get info

```
CLIENT INFO
python3 database/get_client_info.py

ROOM INFO
python3 databse/get_room_info.py
```

- BOOK ROOM
```
python3 book_room.py
```
