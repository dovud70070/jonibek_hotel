import psycopg2


# from config import config


def get_room():
    """ query data from the vendors table """
    conn = None

    try:
        conn = psycopg2.connect(host="localhost",
                                database="hotel",
                                user="hotel",
                                password="hotel")
        cur = conn.cursor()
        cur.execute("SELECT room_id, room_number, room_name, price, is_busy FROM room ORDER BY room_id")
        print("The amount of rooms", cur.rowcount)
        row = cur.fetchone()

        while row is not None:
            print(f"Id {row[0]} Number {row[1]} Name {row[2]} Price {row[3]} Is Busy {row[4]}")
            row = cur.fetchone()

        cur.close()

    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()


if __name__ == "__main__":
    get_room()


def get_room_with_parameter(room_number):
    """ query data from the vendors table """
    conn = None
    row = None
    is_busy = False
    room_id = None
    try:
        conn = psycopg2.connect(host="localhost",
                                database="hotel",
                                user="hotel",
                                password="hotel")
        cur = conn.cursor()
        postgreSQL_select_Query = "SELECT * FROM room where room_number = %s;"
        cur.execute(postgreSQL_select_Query, (room_number))
        row = cur.fetchone()
        is_busy = row[4]
        room_id = row[0]
        while row is not None:
            print(f"Id {row[0]} Number {row[1]} Name {row[2]} Price {row[3]}")
            row = cur.fetchone()

        cur.close()

    except (Exception, psycopg2.DatabaseError) as error:
        print("error", error)
    finally:
        if conn is not None:
            conn.close()
    return is_busy, room_id
